﻿namespace Lesson9
{
    public class WrongLoginException : Exception
    {
        public WrongLoginException() { }
        public WrongLoginException(string message) : base(message) { }
    }

    public class WrongPasswordException : Exception
    {
        public WrongPasswordException() { }
        public WrongPasswordException(string message) : base(message) { }
    }

    public class UserValidator
    {
        public static bool ValidateUser(string login, string password, string confirmPassword)
        {
            if (login.Length >= 20 || login.Contains(" "))
            {
                throw new WrongLoginException("Your login is wrong");
            }

            if (password.Length >= 20 || password.Contains(" ") || !ContainsDigit(password) || password != confirmPassword)
            {
                throw new WrongPasswordException("Your password is wrong");
            }

            return true;
        }

        private static bool ContainsDigit(string input)
        {
            foreach (char c in input)
            {
                if (char.IsDigit(c))
                {
                    return true;
                }
            }
            return false;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input your login:");
            string login = Console.ReadLine();

            Console.WriteLine("Input your password:");
            string password = Console.ReadLine();

            Console.WriteLine("Confirm your password:");
            string confirmPassword = Console.ReadLine();

            try
            {
                bool isValid = UserValidator.ValidateUser(login, password, confirmPassword);
                if (isValid)
                {
                    Console.WriteLine("Registration is successful");
                }
            }
            catch (WrongLoginException e)
            {
                Console.WriteLine($"Error: {e.Message}");
            }
            catch (WrongPasswordException e)
            {
                Console.WriteLine($"Error: {e.Message}");
            }
        }
    }
}